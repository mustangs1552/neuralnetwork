#include "stdafx.h"
#include "NeuralNetwork.h"

/********************** Neuron **********************/

double Neuron::eta = .15;
double Neuron::alpha = .5;

// Sets the weights for each neuron in the next layer on neuron and it's index in it's own layer.
Neuron::Neuron(const unsigned numOutputs, const unsigned neuronIndex)
{
	myIndex = neuronIndex;

	for (unsigned c = 0; c < numOutputs; c++)
	{
		outputWeights.push_back(Connection());
		outputWeights.back().weight = rand() / double(RAND_MAX);
		outputWeights.back().deltaWeight = 1;
	}

	cout << "      Neuron #" << myIndex << " has " << outputWeights.size() << " weight(s)." << endl;
}

 // Takes the input from the node in the previous layer and performs the manipulation of the input and the weight.
//  Then passes that value to the nodes that it's connected to in the next layer.
void Neuron::FeedForward(Layer prevLayer)
{
	double sum = 0;
	for (unsigned i = 0; i < prevLayer.size(); i++) sum += prevLayer[i].PullOutput(myIndex);

	output = TransferFunction(sum);
}

double Neuron::TransferFunction(double x)
{
	//tanh - output range from -1 to 1
	return tanh(x);
}
double Neuron::TransferFunctionDerivative(double x)
{
	// tanh derivative
	return 1.0 - x * x;
}

void Neuron::CalculateOutputGradients(double target)
{
	double delta = target - output;
	gradient = delta * Neuron::TransferFunction(output);
}
void Neuron::CalculateHiddenGradients(const Layer nextLayer)
{
	double dow = 0;
	// Sum our contributions of the errors at the nodes we feed
	for (unsigned i = 0; i < nextLayer.size() - 1; i++)
	{
		dow += outputWeights[i].weight * nextLayer[i].gradient;
	}
	gradient = dow * Neuron::TransferFunctionDerivative(output);
}
void Neuron::UpdateInputWeights(Layer prevLayer)
{
	// The weights to be updated are in the container in the nodes in the preceding layer
	for (unsigned i = 0; i < prevLayer.size() - 1; i++)
	{
		cout << "    Neuron #" << i << endl;
		Neuron node = prevLayer[i];
		double oldDeltaWeight = node.outputWeights[myIndex].deltaWeight;

		// Individual input, magnified by the gradient and train rate. Also add momentum = a function of the prvious delta weight.
		double newDeltaWeight = eta * node.GetOutput() * gradient * alpha * oldDeltaWeight;
		cout << "      eta ============== " << eta << endl;
		cout << "      node.GetOutput() = " << node.GetOutput() << endl;
		cout << "      gradient ========= " << gradient << endl;
		cout << "      alpha ============ " << alpha << endl;
		cout << "      oldDeltaWeight === " << oldDeltaWeight << endl << endl;

		node.outputWeights[myIndex].deltaWeight = newDeltaWeight;
		node.outputWeights[myIndex].weight += newDeltaWeight;
		cout << "      newDeltaWeight === " << newDeltaWeight << endl;
		cout << "      deltaWeight ====== " << node.outputWeights[myIndex].deltaWeight << endl << endl;
	}
}

vector<Connection> Neuron::GetWeights()
{
	return outputWeights;
}

// Send our output to the requesting neuron and apply the corresponding weight.
double Neuron::PullOutput(const unsigned requesterIndex)
{
	return output * outputWeights[requesterIndex].weight;
}
// Force set an output to this neuron (mainly just for setting the initial input for the input neurons).
void Neuron::SetOutput(const double input)
{
	output = input;
}
// Returns the output of this neuron without applying the weight.
double Neuron::GetOutput()
{
	return output;
}

/*********************** Net ************************/

// Creates the net using the topology given and sets up each neuron cerated with the amount of neurons in the next layer and it's index in it's own layer. Then return the results from the output neurons.
Net::Net(const vector<unsigned> topology)
{
	cout << "Generating net with topology (";
	for (unsigned i = 0; i < topology.size(); i++)
	{
		cout << topology[i];
		if (i + 1 >= topology.size()) cout << ")..." << endl;
		else cout << ", ";
	}

	if (topology.size() < 3) cout << "ERROR: Topology must have at least 3 layers." << endl;
	else
	{
		srand(time(NULL));

		for (unsigned i = 0; i < topology.size(); i++)
		{
			layers.push_back(Layer());
			unsigned ii = 0;
			if (i == topology.size() - 1)
			{
				cout << "  Creating the output layer..." << endl;

				for (ii = 0; ii < topology[i]; ii++) layers[i].push_back(Neuron(0, ii));
				cout << "    Created " << ii << " neurons." << endl;
			}
			else
			{
				if (i == 0) cout << "  Creating the input layer..." << endl;
				else cout << "  Creating hidden layer " << i << "/" << topology.size() - 2 << "..." << endl;

				for (ii = 0; ii <= topology[i]; ii++) layers[i].push_back(Neuron(topology[i + 1] + 1, ii));
				cout << "    Created " << ii << " neurons." << endl;
			}

			layers.back().back().SetOutput(1);
		}
	}

	cout << "Net generated." << endl;
}

vector<double> Net::FeedForward(const vector<double> inputs)
{
	if (inputs.size() != layers[0].size() - 1) cout << "ERROR: Too many or too little inputs." << endl;
	else
	{
		// Set the inputs for the input neurons.
		for (unsigned i = 0; i < layers[0].size(); i++)
		{
			if (i == layers[0].size() - 1) layers[0][i].SetOutput(1);
			else layers[0][i].SetOutput(inputs[i]);
		}

		// Start feeding the data through all the neurons.
		for (unsigned i = 1; i < layers.size(); i++)
		{
			for (unsigned ii = 0; ii < layers[i].size(); ii++) layers[i][ii].FeedForward(layers[i - 1]);
		}
	}

	vector<double> results;
	for (unsigned i = 0; i < layers.back().size(); i++) results.push_back(layers.back()[i].GetOutput());
	return results;
}
void Net::BackProp(const vector<double> targets)
{
	// Calculate overall net error
	error = 0.0;
	for (unsigned i = 0; i <= layers.back().size() - 1; i++)
	{
		double delta = targets[i] - layers.back()[i].GetOutput();
		error += delta * delta;
	}
	error /= layers.back().size(); // Get average layer squared
	error = sqrt(error); // RMS

	// Implement a recent average measrument
	recentAverageError = (recentAverageError * recentAverageSmoothinFactor + error) / (recentAverageSmoothinFactor + 1);

	// Calculate output layer gradients
	for (unsigned i = 0; i < layers.back().size(); i++) layers.back()[i].CalculateOutputGradients(targets[i]);

	// Calculate gradients on hidden layers
	for (unsigned i = layers.size() - 2; i > 0; i--)
	{
		for (unsigned ii = 0; ii < layers[i].size(); ii++) layers[i][ii].CalculateHiddenGradients(layers[i + 1]);
	}
	
	// For all layers from outputs to first hidden layer, update connection weights
	for (unsigned i = layers.size() - 1; i > 1; i--)
	{
		if (i >= layers.size()) break;
		
		cout << "Layer #" << i << endl;
		for (unsigned ii = 0; ii <= layers[i].size() - 1; ii++)
		{
			cout << "  Neuron #" << ii << endl;
			layers[i][ii].UpdateInputWeights(layers[i - 1]);
			cout << "  " << layers[i - 1][0].GetWeights()[ii].deltaWeight << endl;
		}
	}
}

void Net::DisplayNet()
{
	for (unsigned i = 0; i < layers.size(); i++)
	{
		cout << "Layer #" << i + 1 << endl;
		for (unsigned ii = 0; ii < layers[i].size(); ii++)
		{
			cout << "  Neuron #" << ii + 1 << endl;
			vector<Connection> connections = layers[i][ii].GetWeights();
			for (unsigned iii = 0; iii < connections.size(); iii++)
			{
				cout << "    Connection #" << iii + 1 << ": Weight: " << connections[i].weight << " | Delta Weight: " << connections[i].deltaWeight << endl;;
			}
		}
	}
}

double Net::GetRecentAverageError()
{
	return recentAverageError;
}