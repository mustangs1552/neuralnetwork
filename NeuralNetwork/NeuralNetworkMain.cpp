// NeuralNetwork.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

int main()
{
	TrainingData data(5);
	vector<unsigned> topology;
	//data.GetTopology(topology);
	topology.push_back(2);
	topology.push_back(4);
	topology.push_back(1);

	cout << endl << "================================================================================" << endl << endl;

	Net net(topology);

	cout << endl << "================================================================================" << endl << endl;

	vector<double> inputs;
	vector<double> targets;
	data.GetGeneratedInputs(inputs);
	data.GetGeneratedTargets(targets);

	unsigned inputI = 0;
	for (unsigned i = 0; i < data.GetTotalValues(); i++)
	{
		cout << "Pass " << i + 1 << ":" << endl;
		cout << "  Recent average error: " << net.GetRecentAverageError() << endl;
		//inputs[inputI]
		vector<double> currInputs;
		currInputs.push_back(1);
		inputI++;
		currInputs.push_back(1);
		inputI++;
		cout << "  Inputs:  " << currInputs[0] << endl;
		for (unsigned i = 1; i < currInputs.size(); i++) cout << "           " << currInputs[i] << endl;
		vector<double> results = net.FeedForward(currInputs);

		cout << "  Results: " << results[0] << endl;
		for (unsigned i = 1; i < results.size(); i++) cout << "           " << results[i] << endl;

		cout << "  Targets: " << targets[i] << endl;
		//for (unsigned i = 1; i < targets.size(); i++) cout << "           " << targets[i] << endl;
		vector<double> currTarget;
		currTarget.push_back(0);
		net.BackProp(currTarget);

		//net.DisplayNet();
		//if ((i + 1) % 5 == 0) net.DisplayNet();
	}

	getchar();
	return 0;
}

