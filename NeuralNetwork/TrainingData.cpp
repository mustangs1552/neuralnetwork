#include "stdafx.h"
#include "TrainingData.h"

TrainingData::TrainingData(unsigned valueCount)
{
	totalValues = valueCount;
	srand(time(NULL));
	SetChosenTopology();
	//GenerateRandomValues();
	GenerateRepeatedValues();
}

void TrainingData::SetChosenTopology()
{
	cout << "Generated topology is (";
	chosenTopology.push_back(2);
	cout << chosenTopology.back() << ", ";

	unsigned randLayers = rand() % 2 + 1;
	for (unsigned i = 0; i < randLayers; i++)
	{
		unsigned randNeurons = rand() % 5 + 1;
		chosenTopology.push_back(randNeurons);
		cout << chosenTopology.back() << ", ";
	}

	chosenTopology.push_back(1);
	cout << chosenTopology.back() << ")" << endl;
}
void TrainingData::GenerateRandomValues()
{
	cout << "Generating inputs and targets..." << endl;
	for (unsigned set = 0; set < totalValues; ++set)
	{
		unsigned randNumOne = rand() % 2;
		generatedInputs.push_back(randNumOne);
		unsigned randNumTwo = rand() % 2;
		generatedInputs.push_back(randNumTwo);

		if (randNumOne == 0 && randNumTwo == 0) generatedTargets.push_back(0);
		else if (randNumOne == 1 && randNumTwo == 1) generatedTargets.push_back(0);
		else if (randNumOne == 0 && randNumTwo == 1) generatedTargets.push_back(1);
		else if (randNumOne == 1 && randNumTwo == 0) generatedTargets.push_back(1);

		cout << "  " << randNumOne << " || " << randNumTwo << " = " << ((generatedTargets.back() == 0) ? "false" : "true");
		cout << endl;
	}
}
void TrainingData::GenerateRepeatedValues()
{
	unsigned currPair = 0;
	for (unsigned i = 0; i < totalValues; i++)
	{
		if (currPair == 0)
		{
			generatedInputs.push_back(0);
			generatedInputs.push_back(0);
			generatedTargets.push_back(0);
		}
		else if (currPair == 1)
		{
			generatedInputs.push_back(1);
			generatedInputs.push_back(1);
			generatedTargets.push_back(0);
		}
		else if (currPair == 2)
		{
			generatedInputs.push_back(0);
			generatedInputs.push_back(1);
			generatedTargets.push_back(1);
		}
		else if (currPair == 3)
		{
			generatedInputs.push_back(1);
			generatedInputs.push_back(0);
			generatedTargets.push_back(1);
		}
		
		currPair++;
		if (currPair > 3) currPair = 0;
	}
}

void TrainingData::GetTopology(vector<unsigned> &topology)
{
	topology = chosenTopology;
}
void TrainingData::GetGeneratedInputs(vector<double> &inputs)
{
	inputs = generatedInputs;
}
void TrainingData::GetGeneratedTargets(vector<double> &targets)
{
	targets = generatedTargets;
}
unsigned TrainingData::GetTotalValues()
{
	return totalValues;
}