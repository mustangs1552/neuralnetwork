#pragma once

#include "stdafx.h"

#ifndef NEURALNETWORK_H
#define NEURALNETWORK_H
#endif

class Neuron;
typedef vector<Neuron> Layer;

struct Connection
{
	double weight;
	double deltaWeight;
};

class Neuron
{
	public:
		Neuron(const unsigned numOutputs, const unsigned neuronIndex);
		void FeedForward(Layer prevLayer);
		double PullOutput(const unsigned requesterIndex);
		void SetOutput(const double output);
		double GetOutput();
		void CalculateOutputGradients(double target);
		void CalculateHiddenGradients(const Layer nextLayer);
		void UpdateInputWeights(Layer prevLayer);
		vector<Connection> GetWeights();
	private:
		unsigned myIndex;
		vector<Connection> outputWeights;
		double output;
		double gradient;
		static double eta;
		static double alpha;

		static double TransferFunction(double x);
		static double TransferFunctionDerivative(double x);
};

class Net
{
	public:
		Net(const vector<unsigned> topology);
		vector<double> FeedForward(const vector<double> inputs);
		void BackProp(const vector<double> targets);
		double GetRecentAverageError();
		void DisplayNet();
	private:
		vector<Layer> layers;
		double error;
		double recentAverageError;
		double recentAverageSmoothinFactor;
};