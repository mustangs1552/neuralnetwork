#pragma once

#include "stdafx.h"

#ifndef TRAININGDATA_H
#define TRAININGDATA_H
#endif

class TrainingData
{
public:
	TrainingData(unsigned valueCount);
	void GetTopology(vector<unsigned> &topology);
	void GetGeneratedInputs(vector<double> &inputs);
	void GetGeneratedTargets(vector<double> &targets);
	unsigned GetTotalValues();
private:
	void SetChosenTopology();
	void GenerateRandomValues();
	void GenerateRepeatedValues();
	vector<unsigned> chosenTopology;
	vector<double> generatedInputs;
	vector<double> generatedTargets;
	unsigned totalValues;
};